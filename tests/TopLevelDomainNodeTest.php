<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tld-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tld\TopLevelDomainNode;
use PHPUnit\Framework\TestCase;

/**
 * TopLevelDomainNodeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tld\TopLevelDomainNode
 *
 * @internal
 *
 * @small
 */
class TopLevelDomainNodeTest extends TestCase
{
	
	/**
	 * The node to test.
	 * 
	 * @var TopLevelDomainNode
	 */
	protected TopLevelDomainNode $_node;
	
	public function testToString() : void
	{
		$object = $this->_node;
		$this->assertEquals('com', $object->__toString());
	}
	
	public function testGetTls() : void
	{
		$this->assertEquals(new ArrayIterator([
			'example' => new TopLevelDomainNode('example', false),
			'mil' => new TopLevelDomainNode('mil', true),
		]), $this->_node->getTldChildrenNodes());
	}
	
	public function testGetTldSuccess() : void
	{
		$this->assertEquals(new TopLevelDomainNode('example', false), $this->_node->getTldChildNode('example'));
	}
	
	public function testGetTldFailed() : void
	{
		$this->assertNull($this->_node->getTldChildNode('com'));
	}
	
	public function testGetNonTlds() : void
	{
		$this->assertEquals(new ArrayIterator([
			'edu' => new TopLevelDomainNode('edu'),
		]), $this->_node->getNonTldChildrenNodes());
	}
	
	public function testGetNonTldSuccess() : void
	{
		$this->assertEquals(new TopLevelDomainNode('edu'), $this->_node->getNonTldChildNode('edu'));
	}
	
	public function testGetNonTldFailed() : void
	{
		$this->assertNull($this->_node->getNonTldChildNode('com'));
	}
	
	public function testIsTldEmpty() : void
	{
		$this->assertTrue($this->_node->isTld(null));
	}
	
	public function testIsTldEmptyStr() : void
	{
		$this->assertTrue($this->_node->isTld(''));
	}
	
	public function testIsTldLv1() : void
	{
		$this->assertTrue($this->_node->isTld('mil'));
	}
	
	public function testIsTldLv2() : void
	{
		$this->assertTrue($this->_node->isTld('example.mil'));
	}
	
	public function testIsTldLv3() : void
	{
		$this->assertFalse($this->_node->isTld('test.example.mil'));
	}
	
	public function testIsTldLv2b() : void
	{
		$this->assertFalse($this->_node->isTld('xkcd.example'));
	}
	
	public function testIsNotTldLv1() : void
	{
		$this->assertFalse($this->_node->isTld('edu'));
	}
	
	public function testIsNotTldLv2() : void
	{
		$this->assertFalse($this->_node->isTld('example.edu'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_node = new TopLevelDomainNode('com', false, [
			new TopLevelDomainNode('example', false),
			new TopLevelDomainNode('mil', true),
		], [
			new TopLevelDomainNode('edu'),
		]);
	}
	
}
