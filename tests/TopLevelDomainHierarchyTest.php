<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tld-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tld\TopLevelDomainHierarchy;
use PhpExtended\Tld\TopLevelDomainNode;
use PHPUnit\Framework\TestCase;

/**
 * TopLevelDomainHierarchyTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tld\TopLevelDomainHierarchy
 *
 * @internal
 *
 * @small
 */
class TopLevelDomainHierarchyTest extends TestCase
{
	
	/**
	 * The hierarchy to test.
	 * 
	 * @var TopLevelDomainHierarchy
	 */
	protected TopLevelDomainHierarchy $_hierarchy;
	
	public function testToString() : void
	{
		$object = $this->_hierarchy;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testGetNodes() : void
	{
		$this->assertEquals(new ArrayIterator([
			'com' => new TopLevelDomainNode('com', false, [
				new TopLevelDomainNode('example'),
			]),
		]), $this->_hierarchy->getTldRootNodes());
	}
	
	public function testGetNodeFound() : void
	{
		$this->assertEquals(new TopLevelDomainNode('com', false, [
			new TopLevelDomainNode('example'),
		]), $this->_hierarchy->getTldRootNode('com'));
	}
	
	public function testGetNodeNotFound() : void
	{
		$this->assertNull($this->_hierarchy->getTldRootNode('edu'));
	}
	
	public function testIsTldEmpty() : void
	{
		$this->assertTrue($this->_hierarchy->isTld(null));
	}
	
	public function testIsTldEmptyStr() : void
	{
		$this->assertTrue($this->_hierarchy->isTld(''));
	}
	
	public function testIsTldDot() : void
	{
		$this->assertTrue($this->_hierarchy->isTld('.'));
	}
	
	public function testIsTldDomain() : void
	{
		$this->assertTrue($this->_hierarchy->isTld('com'));
	}
	
	public function testIsTldDomain2() : void
	{
		$this->assertTrue($this->_hierarchy->isTld('example.com'));
	}
	
	public function testIsNotTldDomain() : void
	{
		$this->assertFalse($this->_hierarchy->isTld('notfound.com'));
	}
	
	public function testIsTldNonSpecifiedDomain() : void
	{
		$this->assertTrue($this->_hierarchy->isTld('edu'));
	}
	
	public function testIsNotTldNonSpecifiedDomain() : void
	{
		$this->assertFalse($this->_hierarchy->isTld('example.edu'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_hierarchy = new TopLevelDomainHierarchy([
			new TopLevelDomainNode('com', false, [
				new TopLevelDomainNode('example'),
			]),
		]);
	}
	
}
