<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tld-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tld;

use ArrayIterator;
use Iterator;

/**
 * TopLevelDomainHierarchy class file.
 * 
 * This class is a simple implementation of the TopLevelDomainHierarchyInterface.
 * 
 * @author Anastaszor
 */
class TopLevelDomainHierarchy implements TopLevelDomainHierarchyInterface
{
	
	/**
	 * All the top level domain roots.
	 * 
	 * @var array<string, TopLevelDomainNodeInterface>
	 */
	protected array $_nodes = [];
	
	/**
	 * Builds a new hierarchy with the given nodes.
	 * 
	 * @param array<integer, TopLevelDomainNodeInterface> $nodes
	 */
	public function __construct(array $nodes = [])
	{
		foreach($nodes as $node)
		{
			$this->addNode($node);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds the given node to the hierarchy.
	 * 
	 * @param TopLevelDomainNodeInterface $node
	 */
	public function addNode(TopLevelDomainNodeInterface $node) : void
	{
		$this->_nodes[$node->getName()] = $node;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainHierarchyInterface::getTldRootNodes()
	 */
	public function getTldRootNodes() : Iterator
	{
		return new ArrayIterator($this->_nodes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainHierarchyInterface::getTldRootNode()
	 */
	public function getTldRootNode(string $domainName) : ?TopLevelDomainNodeInterface
	{
		$domainName = \mb_strtolower($domainName);
		if(!isset($this->_nodes[$domainName]))
		{
			return null;
		}
		
		return $this->_nodes[$domainName];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainHierarchyInterface::isTld()
	 */
	public function isTld(?string $domain) : bool
	{
		// we are at the top of the hierarchy
		// everything that is not known is a tld
		if(null === $domain)
		{
			return true;
		}
		
		$domain = \trim((string) \mb_strtolower($domain), '.');
		$parts = \explode('.', $domain);
		$domain = \array_pop($parts);
		if(isset($this->_nodes[$domain]))
		{
			return $this->_nodes[$domain]->isTld(\implode('.', $parts));
		}
		
		return 0 === \count($parts);
	}
	
}
