<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tld-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tld;

use ArrayIterator;
use Iterator;

/**
 * TopLevelDomainNode class file.
 * 
 * This class is a simple implementation of the TopLevelDomainNodeInterface.
 * 
 * @author Anastaszor
 */
class TopLevelDomainNode implements TopLevelDomainNodeInterface
{
	
	/**
	 * The name of this node.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Whether every direct children is considered as a tld. Subchilds of this
	 * node are not to be considered as TLDs.
	 * 
	 * @var boolean
	 */
	protected bool $_childsAreTlds = true;
	
	/**
	 * The children nodes that are TLDs.
	 * 
	 * @var array<string, TopLevelDomainNodeInterface>
	 */
	protected array $_tldChildren = [];
	
	/**
	 * The children nodes that are NOT TLDs.
	 * 
	 * @var array<string, TopLevelDomainNodeInterface>
	 */
	protected array $_nonTldChildren = [];
	
	/**
	 * Builds a new node with its children nodes.
	 * 
	 * @param string $name
	 * @param boolean $joker
	 * @param array<integer, TopLevelDomainNode> $tlds
	 * @param array<integer, TopLevelDomainNode> $nonTlds
	 */
	public function __construct(string $name, bool $joker = true, array $tlds = [], array $nonTlds = [])
	{
		$this->_name = $name;
		$this->setChildrenAreTlds($joker);
		
		foreach($tlds as $tld)
		{
			$this->addTldNode($tld);
		}
		
		foreach($nonTlds as $nonTld)
		{
			$this->addNonTldNode($nonTld);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets whether the children of this node must be considered as tlds.
	 * 
	 * @param boolean $theyAre
	 * @return TopLevelDomainNodeInterface
	 */
	public function setChildrenAreTlds(bool $theyAre) : TopLevelDomainNodeInterface
	{
		$this->_childsAreTlds = $theyAre;
		
		return $this;
	}
	
	/**
	 * Adds the given node as tld.
	 * 
	 * @param TopLevelDomainNodeInterface $node
	 * @return TopLevelDomainNodeInterface
	 */
	public function addTldNode(TopLevelDomainNodeInterface $node) : TopLevelDomainNodeInterface
	{
		$this->_tldChildren[$node->getName()] = $node;
		
		return $this;
	}
	
	/**
	 * Adds the given node as non-tld.
	 * 
	 * @param TopLevelDomainNodeInterface $node
	 * @return TopLevelDomainNodeInterface
	 */
	public function addNonTldNode(TopLevelDomainNodeInterface $node) : TopLevelDomainNodeInterface
	{
		$this->_nonTldChildren[$node->getName()] = $node;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainNodeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainNodeInterface::getTldChildrenNodes()
	 */
	public function getTldChildrenNodes() : Iterator
	{
		return new ArrayIterator($this->_tldChildren);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainNodeInterface::getTldChildNode()
	 */
	public function getTldChildNode(string $domainName) : ?TopLevelDomainNodeInterface
	{
		$domainName = \mb_strtolower($domainName);
		if(!isset($this->_tldChildren[$domainName]))
		{
			return null;
		}
		
		return $this->_tldChildren[$domainName];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainNodeInterface::getNonTldChildrenNodes()
	 */
	public function getNonTldChildrenNodes() : Iterator
	{
		return new ArrayIterator($this->_nonTldChildren);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainNodeInterface::getNonTldChildNode()
	 */
	public function getNonTldChildNode(string $domainName) : ?TopLevelDomainNodeInterface
	{
		$domainName = \mb_strtolower($domainName);
		if(!isset($this->_nonTldChildren[$domainName]))
		{
			return null;
		}
		
		return $this->_nonTldChildren[$domainName];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainNodeInterface::isTld()
	 */
	public function isTld(?string $domain) : bool
	{
		if(null === $domain)
		{
			return true;
		}
		
		$parts = \explode('.', $domain);
		$top = \array_pop($parts);
		if(empty($top))
		{
			return true;
		}
		
		if(isset($this->_nonTldChildren[$top]))
		{
			return false;
		}
		
		if(isset($this->_tldChildren[$top]))
		{
			return $this->_tldChildren[$top]->isTld(\implode('.', $parts));
		}
		
		if($this->_childsAreTlds)
		{
			return !(0 < \count($parts));
		}
		
		return 0 < \count($parts);
	}
	
}
