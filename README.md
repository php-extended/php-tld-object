# php-extended/php-tld-object

An implementation of the php-tld-interface library.

![coverage](https://gitlab.com/php-extended/php-tld-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-tld-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-tld-object ^8`


## Basic Usage

You may use this library this way to build a hierarchy :

```php

use PhpExtended\Tld\TopLevelDomainHierarchy;
use PhpExtended\Tld\TopLevelDomainNode;

$hierarchy = new TopLevelDomainHierarchy();
$hierarchy->addNode(new TopLevelDomainNode('edu'));
$com = new TopLevelDomainNode('com');
$com->addNode(new TopLevelDomainNode('example');
$hierarchy->addNode($com);

$hierarchy->isTld('example.com'); // true
$hierarchy->isTld('example.edu'); // false

```


## License

MIT (See [license file](LICENSE)).
